<?php
//Event start parameters
$days = 124;
$time = '10:15';

//Setting up default timezone
$timezone = date_default_timezone_set('Europe/Moscow');

//Converting variable "days" to binary string and converting it to an array
function getWeekDaysArray($days){
	return str_split(sprintf('%07b', $days));
}

//Getting event dates for this week and next week
function getEventDates($days, $time){
	$bin = getWeekDaysArray($days);
	foreach ($bin as $key => $value){
		if (intval($value)){
			$resultDates[] = new Datetime("$time {$key} day this week");
			$resultDates[] = new Datetime("$time {$key} day next week");
		}
	}
	return $resultDates;
}

function getNextEventDate($resultDates){
	$now = new Datetime();
	sort($resultDates);

	foreach ($resultDates as $date){
		if ($date > $now){
			return $date;
		}
	}
}

$nextList = getEventDates($days, $time);
$next = getNextEventDate($nextList);
echo $next->format("d.m.Y H:i");
?>